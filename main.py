import base64
import json
import os
import re
import urllib

from bson import *
from bson.json_util import dumps
from bson.objectid import ObjectId
from collections import OrderedDict
from cryptography.fernet import Fernet
from datetime import date, datetime
from flask import *
from flask_pymongo import PyMongo
import numpy as np
from numpy import linalg as LA
from pymongo import MongoClient
import sha3

from methods_utils import *

app = Flask(__name__)

# Conexión a la BD
# Creado por: Daniel Cruzado Poveda

cluster = MongoClient('mongodb+srv://root:' + os.environ['PASS_MONGO'] +
                       '@cluster0-2gbdv.mongodb.net/test?retryWrites=true&w=majority')

DB = cluster['ecu-learning']
users = DB['usuario']
problems_relationships = DB['relacion-problemas']
students_relationships_solved = DB['relacion-alumno']

# Direcciones de la web

@app.route('/')
def index():
    return render_template("login.html")

@app.route('/register')
def register():
    return render_template("register.html")

@app.route('/initial_page')
def initial_page():
    return render_template("initial_page.html")

@app.route('/relationships_of_problems')
def problems_relations():
    return render_template("problems_relations.html")

@app.route('/solved_problems')
def solved_problems():
    return render_template("solved_problems.html")

@app.route('/my_published_problems')
def my_published_problems():
    return render_template("my_published_problems.html")

@app.route('/profile')
def profile():
    return render_template("profile.html")

@app.route('/edit_profile')
def edit_profile():
    return render_template("edit_profile.html")

@app.route('/publish_page')
def publish_page():
    return render_template("publish_page.html")

@app.route('/details_page')
def details_page():
    return render_template("details_page.html")

@app.route('/solve_page')
def solve_page():
    return render_template("solve_page.html")

@app.route('/navbar')
def navbar():
    return render_template("navbar.html")

# Métodos HTTP

@app.route('/login', methods = ['POST'])
def login():
    """
    Realiza el login del usuario con los datos proporcionados.
    Si no encuentra el usuario, devuelve un error 404.
    """
    if request.method == 'POST':
        resp = request.json
        encoded_pass = sha3.sha3_224(resp['contraseña'].encode('utf-8')).hexdigest()
        try:
            usuario = users.find_one({'usuario' : resp['usuario'], 
                                      'contraseña' : encoded_pass})
            if (usuario != None) :
                return('', 200)
            else:
                return('', 404)
        except Exception:
            return ('', 401)

@app.route('/create_user', methods = ['POST'])
def create_user():
    """
    Intenta crear un usuario con los datos recibidos. Si no, devuelve un
    400. Encriptado con sha_3 224
    """
    if request.method == 'POST':
        resp = request.json
        # Primero se comprueba que el usuario no exista ya ni el email (lo realizamos)
        # en caso de que el usuario se las apañe para evitar las restricciones vía cliente
        try:
            user_found = users.find_one(
                {"$or":[ {"usuario": resp['usuario'] },
                {"email": resp['email'] }]}
                )
            if user_found != None:
                return ('Este usuario ya existe', 403)
        except Exception:
            return ("Error en la comprobación de datos", 400)
        encoded_pass = sha3.sha3_224(resp['contraseña'].encode('utf-8')).hexdigest()
        try:
            if resp != None or len(response) > 0:
                users.insert_one({ 'usuario' : resp['usuario'],
                                    'nombre_completo' : resp['nombre_completo'],
                                    'email' : resp['email'],
                                    'rol' : resp['rol'],
                                    'contraseña' : encoded_pass
                })
                return ('Usuario creado correctamente',200)
            else:
                return ('No se ha insertado datos', 400)
        except Exception:
            return ('Error en la inserción de datos',400)

@app.route('/user/username/<username>', methods = ['GET'])
def retrieve_user_by_username(username):
    """
    Busca un usuario por su apodo. Si existe, devuelve un json con
    los datos del mismo. Si no, devuelve None.
    """
    if request.method == 'GET':
        try:
            usuario = users.find_one({"usuario" : username})
            if (usuario != None) :
                resp = {'usuario' : usuario['usuario'],
                "nombre_completo": usuario['nombre_completo'],
                "email": usuario['email'],
                "rol": usuario['rol']}
            else:
                resp = None
            return json.dumps(resp)
        except Exception:
            return ('Usuario no encontrado', 404)


@app.route('/user/email/<email>', methods = ['GET'])
def retrieve_user_by_email(email):
    """
    Busca a un usuario por su email. Si existe, devuelve los datos
    en un json. Si no, devuelve None.
    """
    if request.method == 'GET':
        try:
            usuario = users.find_one({"email" : email})
            if (usuario != None) :
                resp = {'usuario' : usuario['usuario'],
                "nombre_completo": usuario['nombre_completo'],
                "email": usuario['email'],
                "rol": usuario['rol']}
            else:
                resp = None
            return json.dumps(resp)
        except Exception:
            return ('Usuario no encontrado', 404)

@app.route('/user/role/<username>', methods = ['GET'])
def retrieve_user_role(username):
    """
    Dado un nombre de usuario, se devuelve el rol del mismo.
    En caso de no existir el usuario, se devuelve un error 404.
    """
    try:
        usuario = users.find_one({'usuario' : username})
        if (usuario != None):
            return json.dumps(usuario['rol'])
        else:
            return json.dumps(None)
    except Exception:
        return ('El usuario no existe', 404)

@app.route('/profile/<username>', methods = ['GET', 'PUT'])
def retrieve_profile(username):
    """
    Dado un nombre de usuario, se devuelve la información del mismo.
    """
    if request.method == 'GET':
        try:
            usuario = users.find_one({'usuario' : username})
            if (usuario != None):
                response = {
                    'nombre_usuario': usuario['usuario'],
                    'nombre_completo': usuario['nombre_completo'],
                    'email': usuario['email'],
                    'rol': usuario['rol']
                }
                return response
            else:
                return ('Usuario no encontrado', 404)
        except Exception:
            return ('El usuario no existe', 404)
    elif request.method == 'PUT':
        """
        Dado un nombre de usuario, se actualizan los datos
        de perfil del mismo respecto a lo que se haya contestado 
        en el formulario
        """
        resp = request.json
        response = {}
        for field in resp:
            if (resp[field] != None and
            resp[field] != ''):
                response[field] = resp[field]
        try:
            if resp['contraseña'] != '':
                response['contraseña'] = encoded_pass = sha3.sha3_224(
                resp['contraseña'].encode('utf-8')).hexdigest()
        except Exception:
            return ('', 400)
        try:
            old_username = resp['antiguo_usuario']
            del resp['antiguo_usuario']
            users.update_one({"usuario": old_username}, 
            {"$set": response}, upsert = True)

            if resp['rol'] == 'alumno':
                rel_solved = students_relationships_solved.find(
                    {"student_solver":old_username}
                )
                if rel_solved.count() > 0:
                    students_relationships_solved.update({
                        "student_solver":old_username
                    },
                    {"$set": {"student_solver": resp['usuario'] }
                    },
                    upsert = True,
                    multi = True
                    )
            else:
                rel_created = problems_relationships.find(
                    {"autor": old_username}
                )
                if rel_created.count() > 0:
                    problems_relationships.update({
                        "autor":old_username
                    },
                    {"$set": {"autor": resp['usuario'] }
                    },
                    upsert = True,
                    multi = True
                    )
            return ('Cambios realizados correctamente', 200)
        except Exception:
            return ('Error en la actualización', 400)

@app.route('/publish_problem', methods = ['POST'])
def publish_problem():
    """
    Se publica un problema, guardándose los datos del mismo
    en la base de datos. Los datos se recogen del cuerpo 
    de la petición.
    """
    if request.method == 'POST':
        response = {}
        print(request.form)
        for field in request.form:
            try:
                value = float(request.form[field])
            except:
                value = request.form[field]
            response[field] = value
        response['creation_date'] = datetime.today().strftime('%d-%m-%Y')
        print(response['creation_date'])
        try:
            problems_relationships.insert_one(response)
            return redirect("/my_published_problems")
        except Exception:
            return('', 400)

@app.route('/check_equations', methods = ['POST'])
def check_equations():
    """
    Esta función comprueba que los sistemas de ecuaciones
    proporcionados por el usuario no son singulares
    """
    if request.method == 'POST':
        matrix = np.array([[]])
        equations = request.json
        for equation in equations:
            subrow = np.array([])
            for factor in equation:
                subrow = np.append(subrow, 
                int(equation[factor]))
            matrix  = (np.vstack([matrix, subrow]) if 
            matrix.size else subrow)

        matrix = eliminate_independent_terms(matrix)

        try:
            inv_matrix = LA.inv(matrix)
        except LA.LinAlgError:
            return ('Matriz no invertible', 400)
        return ('Ecuación invertible', 200)

@app.route('/published_problems', methods = ['GET'])
def published_problems():
    """
    Devuelve los 15 últimas relaciones de problemas publicados
    """
    if request.method == 'GET':
        try:
            problems = problems_relationships.find().limit(15)
            response = []
            if (problems.count() > 0) :
                for problem in problems:
                    item = {'autor': problem['autor'],
                            'titulo': problem['relation_title'],
                            'fecha_creacion' : problem['creation_date'],
                            'object_id': str(problem['_id'])
                        }
                    response.append(item)
            else:
                response = None
            return json.dumps(response)
        except Exception:
                return ('', 401)

@app.route('/published_problems/<author>', methods = ['GET'])
def published_problems_by_author(author):
    if request.method == 'GET':
        """
        Devuelve las relaciones de problemas publicadas dado
        un autor específico
        """
        try:
            problems = problems_relationships.find({"autor" : author})
            response = []
            if (problems.count() > 0) :
                for problem in problems:
                    item = {'autor': problem['autor'],
                            'titulo': problem['relation_title'],
                            'fecha_creacion' : problem['creation_date'],
                            'object_id': str(problem['_id'])
                        }
                    response.append(item)
            else:
                response = None
            return json.dumps(response)
        except Exception:
                return ('', 401)

@app.route('/published_problems/id/<id>', methods = ['GET', 'DELETE'])
def published_problem_by_id(id):
    if request.method == 'GET':
        try:
            relation = problems_relationships.find_one({"_id" : ObjectId(id)})
            response = {}
            if (relation != None) :
                for field in relation:
                    if field != "_id":
                        response[field] = relation[field]
            else:
                response = None
            return json.dumps(response)
        except Exception:
                return ('', 401)
    if request.method == 'DELETE':
        try:
            students_relationships_solved.delete_many(
                {"relation_id" : ObjectId(id)}
            )
            problems_relationships.find_one_and_delete(
                {"_id" : ObjectId(id)}
            )
            return ('Eliminación realizada', 200)
        except Exception:
            return ('Error al realizar la eliminación', 403)


@app.route('/published_problems/<autor>/<title>', methods = ['GET'])
def published_problem_by_title_and_author(autor,title):
    """
    Devuelve las relaciones de problemas publicadas 
    por un autor, filtrados por nombre de título
    """
    if request.method == 'GET':
        rgx = re.compile('.*' + title + '.*', re.IGNORECASE)
        try:
            problems = problems_relationships.find(
                {
                "autor" : autor,
                "relation_title" : { '$regex': rgx }
                }
                )
            response = None
            if (problems.count() > 0) :
                response = []
                for problem in problems:
                    item = {'autor': problem['autor'],
                            'titulo': problem['relation_title'],
                            'fecha_creacion' : problem['creation_date'],
                            'object_id': str(problem['_id'])
                        }
                    response.append(item)
            return json.dumps(response)
        except Exception:
                return ('', 401)

@app.route('/published_problems/searchFilter/<input>', methods = ['GET'])
def published_problem_by_title_or_name(input):
    """
    Devuelve las relaciones de problemas publicadas 
    por un profesor, filtrados por un filtro específico 
    introducido por el usuario, bien sea el nombre de autor
    o el título de la relación
    """
    if request.method == 'GET':
        rgx = re.compile('.*' + input + '.*', re.IGNORECASE)
        try:
            problems = problems_relationships.find(
                {"$or":[ {"autor": rgx },
                {"relation_title": rgx }]}
                )
            response = None
            if (problems.count() > 0) :
                response = []
                for problem in problems:
                    item = {'autor': problem['autor'],
                            'titulo': problem['relation_title'],
                            'fecha_creacion' : problem['creation_date'],
                            'object_id': str(problem['_id'])
                        }
                    response.append(item)
            return json.dumps(response)
        except Exception:
                return ('', 401)

@app.route('/published_problems/filter/<filter>', methods = ['GET'])
def published_problem_by_filter(filter):
    """
    Devuelve los problemas de publicados ordenados
    por un filtro indicado
    """
    if request.method == 'GET':
        if "_asc" in filter :
            order = 1
            reversion = True
            filter = filter.replace("_asc","")
        else:
            order = -1
            reversion = False
            filter = filter.replace("_desc","")

        try:
            problems = problems_relationships.find().sort([
                (filter, order)
            ])
            response = None
            if (problems.count() > 0) :
                response = []
                for problem in problems:
                    item = {'autor': problem['autor'],
                            'titulo': problem['relation_title'],
                            'fecha_creacion' : problem['creation_date'],
                            'object_id': str(problem['_id'])
                        }
                    response.append(item)
                # El tratamiento de las fechas es un caso concreto
                if filter == 'creation_date':
                    response = sorted(
                        response, 
                        key=lambda item: datetime.strptime(
                            item['fecha_creacion'],
                            '%d-%m-%Y'),
                        reverse = reversion
                        )
            return json.dumps(response)
        except Exception:
                return ('', 400)


@app.route('/solved_problems/<solver>', methods = ['GET'])
def solved_problems_by_solver(solver):
    """
    Devuelve una lista de relaciones de problemas resueltas
    por un alumno
    """
    if request.method == 'GET':
        try:
            relationships_responses = students_relationships_solved.find(
                {"student_solver" : solver}
                )
            final_response = None
            if (relationships_responses.count() > 0) :
                final_response = []
                for response in relationships_responses:
                    relation_data = problems_relationships.find_one(
                        {
                            '_id' : ObjectId(response['relation_id'])      
                        }
                    )
                    response['_id'] = str(response['_id'])
                    response['relation_id'] = str(relation_data['_id'])
                    response['autor'] = relation_data['autor']
                    response['relation_title'] = relation_data['relation_title']
                    final_response.append(response)
            return json.dumps(final_response)
        except Exception:
                return ('', 401)

@app.route('/solved_problems/<solver>/<filter>', methods = ['GET'])
def solved_problems_filtered_by_title(solver, filter):
    """
    Devuelve los relaciones resueltas por un alumno
    filtrados por la entrada introducida. "solver"
    el alumno y "filter" el filtro escrito.
    """
    if request.method == 'GET':
        try:
            relationships_responses = students_relationships_solved.find(
                {"student_solver" : solver}
                )
            rgx = re.compile('.*' + filter + '.*', re.IGNORECASE)
            final_response = None
            if (relationships_responses.count() > 0) :
                final_response = []
                for response in relationships_responses:
                    relation_data = problems_relationships.find_one(
                        {
                            '_id' : ObjectId(response['relation_id']),
                            'relation_title': rgx 
                        }
                    )
                    if relation_data != None:
                        response['_id'] = str(response['_id'])
                        response['relation_id'] = str(relation_data['_id'])
                        response['autor'] = relation_data['autor']
                        response['relation_title'] = relation_data['relation_title']
                        final_response.append(response)
            return json.dumps(final_response)
        except Exception:
                return ('', 401)

@app.route('/solved_problems/responses/<solver>/<relation_id>', methods = ['GET'])
def given_answers(solver,relation_id):
    """
    Devuelve las respuestas de una relación realizada por
    un alumno, dado su nombre de usuario y el id de
    la relación de problemas.
    """
    if request.method == 'GET':
        try:
            relationship_answers = students_relationships_solved.find_one(
                {"student_solver" : solver,
                 "relation_id" : relation_id}
                )
            final_response = {}
            if (relationship_answers != None) :
                for response in relationship_answers:
                    if response != '_id':
                        final_response[response] = relationship_answers[response]
            else:
                final_response = None
            return json.dumps(final_response)
        except Exception:
                return ('', 400)

@app.route('/solved_problems/id/<relation_id>', methods = ['GET'])
def students_answers(relation_id):
    """
    Devuelve qué alumnos respondieron a la relación de problemas,
    las notas que obtuvieron y una media de la misma
    """
    if request.method == 'GET':
        try:
            relationship_answers = students_relationships_solved.find(
                {"relation_id" : relation_id}
                )
            final_response = []
            marks = []
            if (relationship_answers.count() > 0) :
                for response in relationship_answers:
                    resp = dict(response)
                    del resp['_id']
                    final_response.append(resp)
                    marks.append(resp['mark'])
                median = np.mean(marks)
                final_response.append(median)
            else:
                final_response = None
            return json.dumps(final_response)
        except Exception:
                return ('', 400)

@app.route('/solve_page/publish_solved_relationship', methods = ['POST'])
def publish_solved_relationship():
    """
    Publica la solución de una relación de problemas 
    respecto los datos introducidos en formulario.
    """
    if request.method == 'POST':
        response = {}
        for field in request.form:
            response[field] = request.form[field]
        response['creation_date'] = datetime.today().strftime('%d-%m-%Y')
        correct_student_answers(response)
        return redirect("/relationships_of_problems")


def correct_student_answers(answers):
    """
    Función auxiliar que realiza la corrección por pasos
    de las respuestas del alumno.
    """
    number_of_problems = int(answers['number_of_problems'])
    maximum_mark = 10
    total_mark = 0

    # Estructura que guarda el pivote y el paso en el que se hizo
    pivotes = {}

    # Buscamos la relación de problemas
    actual_relation = problems_relationships.find_one(
    {"_id" : ObjectId(answers['relation_id'])}
    )

    # Estos pasos se están usando siguiendo el método de igualación
    for problem_index in range(1, number_of_problems+1):
        problem_index = str(problem_index)
        number_of_expected_steps = int(actual_relation['number_of_steps_expected_'+problem_index])
        number_of_steps_in_chosen_problem = int(answers['number_of_steps_problem_'+problem_index])
        if ((number_of_steps_in_chosen_problem <= (number_of_expected_steps-4))
        or (number_of_steps_in_chosen_problem >= (number_of_expected_steps+4))):
            maximum_mark = 4
        mark_per_problem = maximum_mark / number_of_problems

        equations_transformed = {}
        factor_values = {}
        dimensions = actual_relation['dimensions_'+problem_index]
        
        if dimensions == '2x2':
            num_of_factors = 2
        else:
            num_of_factors = 3

        # Creamos un np.array por cada ecuación
        if dimensions == '2x2':
            equation_1 = np.array([
                actual_relation['matrix_'+problem_index+'_term_x_equation_1'],
                actual_relation['matrix_'+problem_index+'_term_y_equation_1'],
                actual_relation['matrix_'+problem_index+'_term_independent_equation_1']
            ])
            equation_2 = np.array([
                actual_relation['matrix_'+problem_index+'_term_x_equation_2'],
                actual_relation['matrix_'+problem_index+'_term_y_equation_2'],
                actual_relation['matrix_'+problem_index+'_term_independent_equation_2'] 
            ])
            equation_3 = np.array([])
            matrix = np.array([equation_1, equation_2])
            indep_terms = recover_independent_terms(matrix)
            matrix = eliminate_independent_terms(matrix)
            solution = np.linalg.solve(matrix, indep_terms)
            expected_x = solution[0]
            expected_y = solution[1]

        else:
            equation_1 = np.array([
                actual_relation['matrix_'+problem_index+'_term_x_equation_1'],
                actual_relation['matrix_'+problem_index+'_term_y_equation_1'],
                actual_relation['matrix_'+problem_index+'_term_z_equation_1'],
                actual_relation['matrix_'+problem_index+'_term_independent_equation_1'] 
            ])
            equation_2 = np.array([
                actual_relation['matrix_'+problem_index+'_term_x_equation_2'],
                actual_relation['matrix_'+problem_index+'_term_y_equation_2'],
                actual_relation['matrix_'+problem_index+'_term_z_equation_2'],
                actual_relation['matrix_'+problem_index+'_term_independent_equation_2'] 
                ])
            equation_3 = np.array([
                actual_relation['matrix_'+problem_index+'_term_x_equation_3'],
                actual_relation['matrix_'+problem_index+'_term_y_equation_3'],
                actual_relation['matrix_'+problem_index+'_term_z_equation_3'],
                actual_relation['matrix_'+problem_index+'_term_independent_equation_3']
            ])

            matrix = np.array([equation_1, equation_2, equation_3])
            indep_terms = recover_independent_terms(matrix)
            matrix = eliminate_independent_terms(matrix)
            solution = np.linalg.solve(matrix, indep_terms)
            expected_x = solution[0]
            expected_y = solution[1]
            expected_z = solution[2]

        for step_index in range(1, number_of_steps_in_chosen_problem+1):

            mark_per_step = mark_per_problem / number_of_steps_in_chosen_problem
            step_index = str(step_index)
            actual_step = 'problem_'+problem_index+"_step_type_chosen_"+step_index

            if answers[actual_step] == 'pivot' and len(pivotes) < num_of_factors:
                pivotes['factor'] = answers[actual_step+'_pivot_value']
                pivotes['equation'] = answers[actual_step+'_pivot_equation']
                total_mark += mark_per_step
            elif answers[actual_step] == 'pivot' and len(pivotes) >= num_of_factors:
                pivotes['factor'] = answers[actual_step+'_pivot_value']
                pivotes['equation'] = answers[actual_step+'_pivot_equation']

            # Hay que quitar estas funciones, están dando problemas. Ponerlo como estaba
            # originalmente

            if answers[actual_step] == 'transform':
                max_mark_this_step = mark_per_step

                # Guardamos la ecuación escogida
                equation_chosen = answers[actual_step+'_equation_chosen']

                # Y ahora guardamos el np.array
                if equation_chosen == 'equation_1':
                    equation_chosen = equation_1
                elif equation_chosen == 'equation_2':
                    equation_chosen = equation_2
                elif equation_chosen == 'equation_3':
                    equation_chosen = equation_3
                else:
                    equation_chosen = equations_transformed[
                        "transformation_problem_"+problem_index+"_step_"+step_index
                    ]
                
                # Dependiendo de la dimensión, el array de respuesta es uno u otro
                if dimensions == '2x2':
                    answered_equation = np.array([
                        float(answers[actual_step+'_transform_input_x']),
                        float(answers[actual_step+'_transform_input_y']),
                        float(answers[actual_step+'_transform_input_independent'])
                    ])
                else:
                    answered_equation = np.array([
                        float(answers[actual_step+'_transform_input_x']),
                        float(answers[actual_step+'_transform_input_y']),
                        float(answers[actual_step+'_transform_input_z']),
                        float(answers[actual_step+'_transform_input_independent'])
                    ])

                operator = float(answers[actual_step+'_operator_transform'])

                # Comprobamos si es correcta la respuesta del producto/división
                if answers[actual_step+"_transform_choice"] == 'multiplication':
                    expected_equation = np.array([])
                    for position in range(0, equation_chosen.shape[0]):
                        expected_equation = (
                            np.append(expected_equation, equation_chosen[position] * 
                            operator)
                        ) 
                else:
                    expected_equation = np.array([])
                    for position in range(0, equation_chosen.shape[0]):
                        expected_equation = (
                            np.append(expected_equation, equation_chosen[position] / 
                            operator)
                        )
                    
                # Si no nos da el valor correcto, obtiene la mitad de puntuación
                if (expected_equation != answered_equation).all():
                    print('Hay un fallo en el paso: '+step_index)
                    max_mark_this_step -= max_mark_this_step/2 
                total_mark += max_mark_this_step

                equations_transformed["transformation_problem_"+
                problem_index+"_step_"+step_index] = answered_equation

            if answers[actual_step] == 'substitute_factor_by_equation':
                max_mark_this_step = mark_per_step

                # Guardamos la ecuación escogida
                equation_chosen = answers[actual_step+'_equation_factor_sustitution_by_equation']

                # Guardamos la incógnita escogida
                factor_chosen = answers[actual_step+'_factor_to_substitute_by_equation_value']

                if factor_chosen == 'x':
                    factor_index = 0
                elif factor_chosen == 'y':
                    factor_index = 1
                else:
                    factor_index = 2

                # Y ahora guardamos el np.array
                if equation_chosen == 'equation_1':
                    equation_chosen = equation_1
                elif equation_chosen == 'equation_2':
                    equation_chosen = equation_2
                elif equation_chosen == 'equation_3':
                    equation_chosen = equation_3
                else:
                    equation_chosen = equations_transformed[
                        equation_chosen
                    ]
                
                expected_equation_1 = np.array([])
                expected_equation_2 = np.array([])

                if equation_chosen[factor_index] != 0:
                # Creo las posibles ecuaciones resultado
                    for position in range(0,equation_chosen.shape[0]):           
                        expected_equation_1 = (
                            np.append(expected_equation_1,
                            equation_chosen[position] / 
                            equation_chosen[factor_index])
                        )
                        expected_equation_2 = (
                            np.append(expected_equation_2, 
                            (-1) *
                            equation_chosen[position] / 
                            equation_chosen[factor_index])
                        )
                        if (position != equation_chosen.shape[0]-1):
                            expected_equation_1[position] = (
                                expected_equation_1[position] *
                                (-1)
                            )
                            expected_equation_2[position] = (
                                expected_equation_1[position] *
                                (-1)
                            )
                    expected_equation_1[factor_index] = 0
                    expected_equation_2[factor_index] = 0                   

                # Dependiendo de la dimensión, el array de respuesta es uno u otro
                if dimensions == '2x2':
                    answered_equation = np.array([
                        float(answers[actual_step+'_factor_by_equation_input_x']),
                        float(answers[actual_step+'_factor_by_equation_input_y']),
                        float(answers[actual_step+'_factor_by_equation_independent'])
                    ])
                else:
                    answered_equation = np.array([
                        float(answers[actual_step+'_factor_by_equation_input_x']),
                        float(answers[actual_step+'_factor_by_equation_input_y']),
                        float(answers[actual_step+'_factor_by_equation_input_z']),
                        float(answers[actual_step+'_factor_by_equation_independent'])
                    ])

                if ((expected_equation_1 != answered_equation).all() or
                    (expected_equation_2 != answered_equation).all()):
                    print('Hay un fallo en el paso: '+step_index)
                    max_mark_this_step -= max_mark_this_step/2

                equations_transformed[
                    "factor_substitute_problem_"+problem_index+"_step_"+step_index
                    ] = answered_equation
                
                total_mark += max_mark_this_step

            if answers[actual_step] == 'substitute_factor_in_equation':
                max_mark_this_step = mark_per_step

                factor_chosen = answers[actual_step+'_factor_to_substitute_value']
                equation_chosen = answers[actual_step+'_equation_factor_sustitution_value']
                most_recent_equation_transformed = np.array([])

                # Dependiendo de la dimensión, el array de respuesta es uno u otro
                if dimensions == '2x2':
                    answered_equation = np.array([
                        float(answers[actual_step+'_factor_in_equation_input_x']),
                        float(answers[actual_step+'_factor_in_equation_input_y']),
                        float(answers[actual_step+'_factor_in_equation_input_independent'])
                    ])
                else:
                    answered_equation = np.array([
                        float(answers[actual_step+'_factor_in_equation_input_x']),
                        float(answers[actual_step+'_factor_in_equation_input_y']),
                        float(answers[actual_step+'_factor_in_equation_input_z']),
                        float(answers[actual_step+'_factor_in_equation_input_independent'])
                    ])
                
                if equation_chosen == 'equation_1':
                    most_recent_equation_transformed = equation_1
                elif equation_chosen == 'equation_2':
                    most_recent_equation_transformed = equation_2
                elif equation_chosen == 'equation_3':
                    most_recent_equation_transformed = equation_3
                elif ("transformation_problem_" in equation_chosen):
                    most_recent_equation_transformed = equations_transformed[
                        equation_chosen
                        ]

                expected_equation = np.array([])
                if factor_chosen == 'x':
                    factor_index = 0
                elif factor_chosen == 'y':
                    factor_index = 1
                else:
                    factor_index = 2
                factor_chosen = (factor_index, factor_values[factor_chosen])
                
                expected_equation = substitute_factor(factor_chosen,
                most_recent_equation_transformed)
                
                if (expected_equation != answered_equation).all():
                    print('Hay un fallo en el paso: '+step_index)
                    max_mark_this_step -= max_mark_this_step/2                
                        
                equations_transformed["transformation_problem_"+
                problem_index+"_step_"+step_index] = answered_equation
                
                total_mark += max_mark_this_step 
            
            if answers[actual_step] == 'solve':
                factor_chosen = answers[actual_step+'_solve_value']
                if factor_chosen == "x":
                    factor_values['x'] = float(answers[actual_step+'_result_input_x'])

                    if (float(answers[actual_step+'_result_input_x']) == expected_x):
                        total_mark += mark_per_step
                elif factor_chosen == 'y':  
                    factor_values['y'] = float(answers[actual_step+'_result_input_y'])
                    if (float(answers[actual_step+'_result_input_y']) == expected_y):
                        total_mark += mark_per_step
                else:
                    factor_values['z'] = float(answers[actual_step+'_result_input_z'])
                    if (float(answers[actual_step+'_result_input_z']) == expected_z):
                        total_mark += mark_per_step
            
            if answers[actual_step] == 'equalise_two_equations':
                first_equation_chosen = answers[actual_step+'_equalise_first_equation']
                second_equation_chosen = answers[actual_step+'_equalise_second_equation']
                max_mark_this_step = mark_per_step

                if first_equation_chosen == 'equation_1':
                    first_equation_chosen = equation_1
                elif first_equation_chosen == 'equation_2':
                    first_equation_chosen = equation_2
                elif first_equation_chosen == 'equation_3':
                    first_equation_chosen = equation_3
                else:
                    first_equation_chosen = equations_transformed[
                        first_equation_chosen
                    ]

                if second_equation_chosen == 'equation_1':
                    second_equation_chosen = equation_1
                elif second_equation_chosen == 'equation_2':
                    second_equation_chosen = equation_2
                elif second_equation_chosen == 'equation_3':
                    second_equation_chosen = equation_3
                else:
                    second_equation_chosen = equations_transformed[
                        second_equation_chosen
                    ]

                if dimensions == '2x2':
                    answered_equation = np.array([
                        float(answers[actual_step+'_transform_input_x']),
                        float(answers[actual_step+'_transform_input_y']),
                        float(answers[actual_step+'_transform_input_independent'])
                    ])
                else:
                    answered_equation = np.array([
                        float(answers[actual_step+'_transform_input_x']),
                        float(answers[actual_step+'_transform_input_y']),
                        float(answers[actual_step+'_transform_input_z']),
                        float(answers[actual_step+'_transform_input_independent'])
                    ])

                if pivotes:
                    expected_equation_1 = first_equation_chosen - second_equation_chosen
                    expected_equation_1[len(expected_equation_1)-1] = ((-1) *
                    expected_equation_1[len(expected_equation_1)-1])
                    expected_equation_2 = np.array(expected_equation_1)
                    for index in range(0,len(expected_equation_2)):
                        expected_equation_2[index] = (-1) * expected_equation_2[index]

                else:
                    expected_equation_1 = np.array([0])
                    expected_equation_2 = np.array([0])

                equations_transformed["transformation_problem_"+
                problem_index+"_step_"+step_index] = answered_equation

                if ((expected_equation_1 != answered_equation).all() or
                (expected_equation_2 != answered_equation).all()):
                    print('Hay un fallo en el paso: '+step_index)
                    max_mark_this_step = 0

                total_mark += max_mark_this_step

            if answers[actual_step] == 'operate':
                print(answers)
                first_equation_chosen = answers[actual_step+'operate_first_equation']
                second_equation_chosen = answers[actual_step+'operate_second_equation']
                max_mark_this_step = mark_per_step

                if first_equation_chosen == 'equation_1':
                    first_equation_chosen = equation_1
                elif first_equation_chosen == 'equation_2':
                    first_equation_chosen = equation_2
                elif first_equation_chosen == 'equation_3':
                    first_equation_chosen = equation_3
                elif ("transformation_problem_" in first_equation_chosen):
                    first_equation_chosen = equations_transformed[
                        first_equation_chosen
                    ]

                if second_equation_chosen == 'equation_1':
                    second_equation_chosen = equation_1
                elif second_equation_chosen == 'equation_2':
                    second_equation_chosen = equation_2
                elif second_equation_chosen == 'equation_3':
                    second_equation_chosen = equation_3
                elif ("transformation_problem_" in second_equation_chosen):
                    second_equation_chosen = equations_transformed[
                        second_equation_chosen
                    ]

                if dimensions == '2x2':
                    answered_equation = np.array([
                        float(answers[actual_step+'_operation_input_x']),
                        float(answers[actual_step+'_operation_input_y']),
                        float(answers[actual_step+'_operation_input_independent'])
                    ])
                else:
                    answered_equation = np.array([
                        float(answers[actual_step+'_operation_input_x']),
                        float(answers[actual_step+'_operation_input_y']),
                        float(answers[actual_step+'_operation_input_z']),
                        float(answers[actual_step+'_operation_input_independent'])
                    ])
                
                if pivotes:
                    expected_equation_1 = first_equation_chosen - second_equation_chosen
                    expected_equation_1[len(expected_equation_1)-1] = ((-1) *
                    expected_equation_1[len(expected_equation_1)-1])
                    expected_equation_2 = np.array(expected_equation_1)
                    for index in range(0,len(expected_equation_2)):
                        expected_equation_2[index] = (-1) * expected_equation_2[index]

                else:
                    expected_equation_1 = np.array([0])
                    expected_equation_2 = np.array([0])

                equations_transformed["transformation_problem_"+
                problem_index+"_step_"+step_index] = answered_equation

                if ((expected_equation_1 != answered_equation).all() or
                (expected_equation_2 != answered_equation).all()):
                    print('Hay un fallo en el paso: '+step_index)
                    max_mark_this_step = 0

                total_mark += max_mark_this_step

            if answers[actual_step] == 'substitute_factor_equationed_in_equation':
                # Esto está orientado al método de sustitución, y es sustituir una
                # incógnita sustituida por una ecuación en otra ecuación
                factor_chosen = answers[actual_step+'_factor_equationed_to_substitute_value']
                equation_chosen = answers[actual_step+'_equation_factor_sustitution_value']

                if equation_chosen == 'equation_1':
                    equation_chosen = equation_1
                elif equation_chosen == 'equation_2':
                    equation_chosen = equation_2
                elif equation_chosen == 'equation_3':
                    equation_chosen = equation_3
                else:
                    equation_chosen = equations_transformed[
                        equation_chosen
                    ]
                equation_factor_chosen = equations_transformed[
                        factor_chosen
                    ]

                if dimensions == '2x2':
                    answered_equation = np.array([
                        float(answers[actual_step+'_factor_equationed_input_x']),
                        float(answers[actual_step+'_factor_equationed_input_y']),
                        float(answers[actual_step+'_factor_equationed_input_independent'])
                    ])
                else:
                    answered_equation = np.array([
                        float(answers[actual_step+'_factor_equationed_input_x']),
                        float(answers[actual_step+'_factor_equationed_input_y']),
                        float(answers[actual_step+'_factor_equationed_input_z']),
                        float(answers[actual_step+'_factor_equationed_input_independent'])
                    ])
                
                if pivotes:
                    if pivotes['factor'] == 'x':
                        factor_index = 0
                    elif pivotes['factor'] == 'y':
                        factorindex = 1
                    else:
                        factor_index = 2

                    expected_equation = substitute_equation_in_factor(factor_index, equation_chosen,
                    equation_factor_chosen)
                else:
                    expected_equation = np.array([0])

                # Si no nos da el valor correcto, obtiene la mitad de puntuación
                if (expected_equation != answered_equation).all():
                    print('Hay un fallo en el paso: '+step_index)
                    max_mark_this_step -= max_mark_this_step/2 
                total_mark += max_mark_this_step

                equations_transformed["transformation_problem_"+
                problem_index+"_step_"+step_index] = answered_equation


    print("Nota final")
    print(total_mark)
    answers['mark'] = total_mark
    # Lo último de todo, insertar la colección.
    students_relationships_solved.insert_one(answers)
  

if __name__ == '__main__':
    app.run(host='127.0.0.1',port=80,debug=True)
