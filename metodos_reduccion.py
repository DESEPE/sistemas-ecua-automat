# coding=utf-8

import numpy as np 
from methods_utils import recover_independent_terms

# Creado por: Daniel Cruzado Poveda

"""
Dada una incógnita elegida ( dependiendo si la matriz se compone
de 2 o 3 incógnitas, pudiendo seleccionar 0, 1 o 2, respectivamente a x, y, z),
dada una fila elegida, y dada una matriz, se devuelve la matriz resultante tras
sustituir (si son 2 incógnitas, devuelve un array). La matriz incluye el término 
independiente.
"""
def reduction_method(chosen_factor, chosen_row, matrix):
    ind_terms = recover_independent_terms(matrix)
    num_rows = matrix.shape[0]
    num_cols = matrix.shape[1]
    count_positive = 0
    pivot_factors = []
    resulting_matrix = np.array([[]])
    
    """
    Si el número de filas es 2, entonces podemos reducir
    a una ecuación única. Si es 3, entonces hay que devolver
    una matriz
    """
    if num_rows == 2:
        # Guardamos los términos de la incógnita
        for row in range(0,num_rows):
            pivot_factors.append(int(matrix[row][chosen_factor]))

        # Ahora podemos obtener el mcm del pivote
        lcm_pivot = np.lcm.reduce(pivot_factors)
        row_1 = np.array([])
        row_2 = np.array([])

        # Ya tenemos el mcm. Ahora hacemos las operaciones pertinentes
        resulting_matrix = multiply_lcm(num_rows, num_cols, matrix, lcm_pivot, chosen_factor)
        row_1 = resulting_matrix[0]
        row_2 = resulting_matrix[1]

        # En caso de tener que los dos son positivos, sumo. Si no, resto.
        if row_1[0] == row_2[0]:
            resulting_matrix = np.subtract(row_1, row_2)
        else:
            resulting_matrix = np.add(row_1, row_2)

    elif num_rows == 3:
        # Guardamos los términos de la incógnita
        for row in range(0,num_rows):
            pivot_factors.append(matrix[row][chosen_factor])

        # Ahora podemos obtener el mcm del pivote
        lcm_pivot = np.lcm.reduce(pivot_factors)

        # Ya tenemos el mcm. Ahora hacemos las operaciones pertinentes
        submatrix = multiply_lcm(num_rows, num_cols, matrix, lcm_pivot, chosen_factor)

        # Hago una lista por compresión que coja los otros dos valores que no sean
        # la fila escogida
        operating_rows = [index for index in range(0,num_rows) if index != chosen_row]
        # Queda operar y devolver la matriz resultado

        resulting_equation_1 = reduction_method(chosen_factor, chosen_row, 
        np.array([submatrix[chosen_row], submatrix[operating_rows[0]]]))
        resulting_equation_2 = reduction_method(chosen_factor, chosen_row, 
        np.array([submatrix[chosen_row], submatrix[operating_rows[1]]]))

        resulting_matrix = np.vstack((resulting_equation_1, resulting_equation_2))

    return resulting_matrix

# Método auxiliar que devuelve la matriz tras multiplicar con el pivote    
def multiply_lcm(num_rows, num_cols, matrix, lcm_pivot, chosen_factor):
    resulting_matrix = np.array([[]])
    for row in range(0, num_rows):
        actual_row = np.array([])
        for col in range(0, num_cols):
            actual_row = np.append(actual_row, (lcm_pivot/matrix[row][chosen_factor]) * matrix[row][col])
        resulting_matrix  = np.vstack([resulting_matrix, actual_row]) if resulting_matrix.size else actual_row
    return resulting_matrix

# if __name__ == "__main__":
#     res_1 = reduction_method(0, 0, np.array([ [1,2,3], [2,3,4] ]) )
#     ind_1 = recover_independent_terms(res_1)
#     res_1 = eliminate_independent_terms(res_1)
#     result = solve_equation(res_1, ind_1)
#     print(result)
