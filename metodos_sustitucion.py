# coding=utf-8
import numpy as np

from methods_utils import *

# Creado por: Daniel Cruzado Poveda

'''
Dada una incógnita (x es 0, y es 1 y z es 2), dada una fila de la
matriz (0-indexada), y dada la matriz,
aplicamos el método de sustituión para un pivote. Se devuelve una
matriz con los valores resultantes, incluyendo en la última columna
los términos independientes.
'''
def substitute_pivot(chosen_var, chosen_row, matrix):
    '''
    Vamos si estamos dentro de los límites válidos de la matriz. No más de 3
    variables y la fila debe estar dentro de las dimensiones de la matriz.
    '''
    ind_value = recover_independent_terms(matrix)
    num_matrix_row = matrix.shape[0]
    if (chosen_var > -1 and chosen_var < 3) and (chosen_row > -1 and chosen_row < num_matrix_row):
        '''
        Transformamos la incógnita elegida sustituyéndola por un nuevo nparray. Tiene que estar
        ordenado, de forma que el último término sea el término independiente. Tengo que recorrer
        los todos los valores menos la variable elegida de la matriz, y negarlos. Luego formar el
        nparray con el término independiente de la fila del t. independiente asociado.
        '''
        num_var = matrix.shape[1]
        # Nos hará falta en caso de que hayamos de hacer submatrices
        submatrix = np.array([[]])
        pivot_row = np.array([])
        # Recorro las distintas incógnitas
        for var in range(0,num_var): 
            # Si mi incógnita no es la que he escogido, la puedo añadir
            if var == chosen_var:   
                pivot_row = np.append(pivot_row, 0)
            elif var != num_var-1: 
                #Hago la transformación y la añado
                transform_val = float(-1 * matrix[chosen_row][var] / matrix[chosen_row][chosen_var])
                #Cuidado porque append no transforma, sólo devuelve resultado
                pivot_row = np.append(pivot_row, transform_val)
            else:
                pivot_row = np.append(pivot_row, matrix[chosen_row][var])

        # Ya tengo mi ecuación pivote, donde la incógnita escogida a sustituir vale 0
        # en nuesta ecuación pivote. Ahora toca sustituir en los términos adecuados
        for row in range(0,num_matrix_row):
            if not np.array_equal(matrix[row], matrix[chosen_row]):
                substitution_row = np.dot(pivot_row, matrix[row][chosen_var])
                # Ahora que tengo el array resultante, tengo que operar el resto de la fila
                new_row = np.array([])
                for var in range(0,num_var):
                    if var == chosen_var:
                        new_row = np.append(new_row, 0)
                    elif var != num_var-1:
                        new_row = np.append(new_row, substitution_row[var] + matrix[row][var])
                    else:
                        new_row = np.append(new_row, matrix[row][var] - substitution_row[var])
                submatrix = np.vstack([submatrix, new_row]) if submatrix.size else new_row

        return submatrix


if __name__ == '__main__':
    matrix = np.array([[1.0, 2.0, 1.0], [1.0, 1.0, 1.0], [1.0, 1.0, 3.0]])
    indep_terms = np.array([4.0, 3.0, 5.0])
    x = np.linalg.solve(matrix, indep_terms)
    print(x)
