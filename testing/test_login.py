import bson
import json
import os
import unittest

from flask import Flask
from pymongo import MongoClient

# Creado por Daniel Cruzado Poveda

# Obviamente necesitamos la aplicación para probarla
from main import app

class FlaskTest(unittest.TestCase):

    # Comprobamos si la conexión se hace correctamente
    def test_index(self):
        tester = app.test_client(self)
        response = tester.get('/', content_type="html/text")
        self.assertEqual(response.status_code, 200)

    # Prueba de un inicio de sesión con unos credenciales
    # válidos
    def test_correct_login(self):
        client = app.test_client(self)
        sent_data = {
            'usuario': 'elprofesor',
            'contraseña': 'contraseña'
        }
        response = client.post('/login', data=json.dumps(sent_data), content_type='application/json')
        self.assertEqual(response.status_code, 200)

    # Prueba de un inicio de sesión con credenciales no válidos,
    # se espera un error 404
    def test_incorrect_login(self):
        client = app.test_client(self)
        sent_data = {
            'usuario': 'antonio',
            'contraseña': 'caramelo'
        }
        response = client.post('/login', data=json.dumps(sent_data), content_type='application/json')
        self.assertEqual(response.status_code, 404)

    # Prueba de inicio de sesión sin especificar credenciales
    def test_not_defined_credentials_login(self):
        client = app.test_client(self)
        sent_data = {
            'usuario': '',
            'contraseña': ''
        }
        response = client.post('/login', data=json.dumps(sent_data), content_type='application/json')
        self.assertEqual(response.status_code, 404)

if __name__ == "__main__":
    unittest.main()
