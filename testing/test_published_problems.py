import bson
import json
import os
import unittest

from flask import Flask
from pymongo import MongoClient

# Creado por Daniel Cruzado Poveda

# Obviamente necesitamos la aplicación para probarla
from main import app

class FlaskTest(unittest.TestCase):

    # Comprobamos si la conexión se hace correctamente
    # a los problemas publicados
    def test_access_relations_published(self):
        tester = app.test_client(self)
        response = tester.get('/relationships_of_problems', 
        content_type="html/text")
        self.assertEqual(response.status_code, 200)

    # Comprobamos si la conexión se hace correctamente
    # a los problemas publicados por un profesor específico
    def test_index_access_relations_professor(self):
        tester = app.test_client(self)
        response = tester.get('/my_published_problems', 
        content_type="html/text")
        self.assertEqual(response.status_code, 200)

    # No testeable con request.form <:/
    # # Prueba de una creación de problemas con datos válidos
    # def test_publish_relation(self):
    #     client = app.test_client(self)
    #     sent_data = {
    #         "autor":"elprofesor",
    #         "number_of_problems": "1",
    #         "relation_title":"Relación 2012",
    #         "title_problem_1":"Problema 1",
    #         "number_of_steps_expected_1":"6",
    #         "method_1":"Sustitución",
    #         "statement_1":"Ayyy lmao",
    #         "dimensions_1":"2x2",
    #         "matrix_1_term_x_equation_1":"1",
    #         "matrix_1_term_y_equation_1":"2",
    #         "matrix_1_term_independent_equation_1": "3",
    #         "matrix_1_term_x_equation_2":"2",
    #         "matrix_1_term_y_equation_2":"2",
    #         "matrix_1_term_independent_equation_2":"4"
    #     }
    #     response = client.post('/publish_problem',
    #     form=sent_data,
    #     content_type='application/json')
    #     self.assertEqual(response.status_code, 200)


    # # Prueba de llamada a la búsqueda de problemas
    # # publicados. 
    # def test_published_problems(self):
    #     tester = app.test_client(self)
    #     response = tester.get('/published_problems', 
    #     content_type="html/text")
    #     self.assertEqual(response.status_code, 200)
    #     response_data = json.loads(response.data.decode('utf-8'))
    #     self.assertLessEqual(len(response_data), 15)
    
    # # Prueba de llamada de problemas publicados por
    # # un profesor existente en la aplicación
    # def test_retrieve_problems_by_existing_professor(self):
    #     tester = app.test_client(self)
    #     response = tester.get('/published_problems/elprofesor', 
    #     content_type='application/json')
    #     self.assertEqual(response.status_code, 200)
    #     data_response = response.data.decode('utf-8')
    #     self.assertIn('autor', data_response)
    #     self.assertIn('titulo', data_response)
    #     self.assertIn('fecha_creacion', data_response)
    
    # # Prueba de llamada de problemas publicados
    # # por un profesor inexistente en la aplicación
    # def test_retrieve_problems_by_nonexistent_professor(self):
    #     tester = app.test_client(self)
    #     response = tester.get('/published_problems/profesormuyreal', 
    #     content_type='application/json')
    #     self.assertEqual(response.status_code, 200)
    #     data_response = response.data.decode('utf-8')
    #     self.assertIn('null', data_response)
    
    # # Prueba de búsqueda de problema con una id 
    # # específica
    # def test_retrieve_problem_with_specific_id(self):
    #     tester = app.test_client(self)
    #     problem_id = "5ec62f34bbddd60ce03a97e5"
    #     response = tester.get('/published_problems/id/'+problem_id, 
    #     content_type='application/json')
    #     self.assertEqual(response.status_code, 200)

    # # Prueba de búsqueda de problema con una id 
    # # específica que no está en la aplicación
    # def test_retrieve_problem_with_nonexistent_id(self):
    #     tester = app.test_client(self)
    #     problem_id = "eeeeeeeeeeeeeeee"
    #     response = tester.get('/published_problems/id/'+problem_id, 
    #     content_type='application/json')
    #     self.assertEqual(response.status_code, 200)
    #     data_response = response.data.decode('utf-8')
    #     self.assertIn('null', data_response)

    # # Prueba de eliminación de problema con una id 
    # # específica que está en la aplicación
    # # MUY IMPORTANTE: Para que esta prueba devuelva
    # # un código 200, el elemento debe existir...
    # def test_delete_existent_problem(self):
    #     tester = app.test_client(self)
    #     problem_id = "5ec7f4f4b1921d863f0ae700"
    #     response = tester.delete('/published_problems/id/'+problem_id, 
    #     content_type='application/json')
    #     self.assertEqual(response.status_code, 200)
    
    # Prueba de eliminación de problema con una id 
    # que no está en la aplicación
    def test_delete_nonexistent_problem(self):
        tester = app.test_client(self)
        problem_id = "sendhelp"
        response = tester.delete('/published_problems/id/'+problem_id, 
        content_type='application/json')
        self.assertEqual(response.status_code, 403)
    
    # Prueba de búsqueda de un problema por autor
    # y nombre de título existentes en la aplicación
    def test_retrieve_problems_by_author_and_title(self):
        tester = app.test_client(self)
        author = 'elprofesor'
        title_relation = 'Igualación 3x3'
        response = tester.get('/published_problems/'+author+
        '/'+title_relation, content_type='application/json')
        self.assertEqual(response.status_code, 200)

    # Prueba de búsqueda de un problema por autor
    # y nombre de título inexistentes en la aplicación
    def test_retrieve_problems_by_nonexistent_author_and_title(self):
        tester = app.test_client(self)
        author = 'elpitulas'
        title_relation = 'Iguana'
        response = tester.get('/published_problems/'+author+
        '/'+title_relation, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertIn('null', response.data.decode('utf-8'))

    # Prueba de búsqueda de un problema por autor
    # (mediante el buscador de la página de problemas)
    def test_retrieve_problems_by_autor_filter(self):
        tester = app.test_client(self)
        author = 'elprofesor'
        response = tester.get('/published_problems/searchFilter/'+
        author, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertIn(author, response.data.decode('utf-8'))
    
    # Prueba de búsqueda de un problema por título
    # (mediante el buscador de la página de problemas)
    def test_retrieve_problems_by_title_filter(self):
        tester = app.test_client(self)
        title = 'Igualación'
        response = tester.get('/published_problems/searchFilter/'+
        title, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Igualaci', response.data.decode('utf-8'))

    # Prueba de búsqueda de un problema por un autor
    # o título inxistente (mediante el buscador de la página
    # de problemas)
    def test_retrieve_problems_using_date_asc_filter(self):
        tester = app.test_client(self)
        title = 'Rabanillo'
        response = tester.get('/published_problems/searchFilter/'+
        title, content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertIn('null', response.data.decode('utf-8'))
    
    # Prueba de búsqueda de un problema mediante filtro de fecha
    # ascendente(los filtros son: creation_date_asc/desc, 
    # relation_title_asc/desc, autor_asc/desc)
    def test_retrieve_problems_using_date_asc_filter(self):
        tester = app.test_client(self)
        filtrator = 'creation_date_asc'
        response = tester.get('/published_problems/filter/'+
        filtrator, content_type='application/json')
        self.assertEqual(response.status_code, 200)

    # Prueba de búsqueda de un problema mediante filtro de fecha
    # descendente(los filtros son: creation_date_asc/desc, 
    # relation_title_asc/desc, autor_asc/desc)
    def test_retrieve_problems_using_date_desc_filter(self):
        tester = app.test_client(self)
        filtrator = 'creation_date_desc'
        response = tester.get('/published_problems/filter/'+
        filtrator, content_type='application/json')
        self.assertEqual(response.status_code, 200)

    # Prueba de búsqueda de un problema mediante filtro de titulo
    # ascendente(los filtros son: creation_date_asc/desc, 
    # relation_title_asc/desc, autor_asc/desc)
    def test_retrieve_problems_using_title_asc_filter(self):
        tester = app.test_client(self)
        filtrator = 'relation_title_asc'
        response = tester.get('/published_problems/filter/'+
        filtrator, content_type='application/json')
        self.assertEqual(response.status_code, 200)

    # Prueba de búsqueda de un problema mediante filtro de fecha
    # descendente(los filtros son: creation_date_asc/desc, 
    # relation_title_asc/desc, autor_asc/desc)
    def test_retrieve_problems_using_title_desc_filter(self):
        tester = app.test_client(self)
        filtrator = 'relation_title_desc'
        response = tester.get('/published_problems/filter/'+
        filtrator, content_type='application/json')
        self.assertEqual(response.status_code, 200)

    # Prueba de búsqueda de un problema mediante filtro de autor
    # ascendente(los filtros son: creation_date_asc/desc, 
    # relation_title_asc/desc, autor_asc/desc)
    def test_retrieve_problems_using_autor_asc_filter(self):
        tester = app.test_client(self)
        filtrator = 'autor_asc'
        response = tester.get('/published_problems/filter/'+
        filtrator, content_type='application/json')
        self.assertEqual(response.status_code, 200)

    # Prueba de búsqueda de un problema mediante filtro de autor
    # descendente(los filtros son: creation_date_asc/desc, 
    # relation_title_asc/desc, autor_asc/desc)
    def test_retrieve_problems_using_nonexistent_filter(self):
        tester = app.test_client(self)
        filtrator = 'autor_desc'
        response = tester.get('/published_problems/filter/'+
        filtrator, content_type='application/json')
        self.assertEqual(response.status_code, 200)


if __name__ == "__main__":
    unittest.main()
