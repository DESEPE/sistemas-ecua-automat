import bson
import json
import os
import unittest

from flask import Flask
from pymongo import MongoClient

# Creado por Daniel Cruzado Poveda

# Obviamente necesitamos la aplicación para probarla
from main import app

class FlaskTest(unittest.TestCase):

    # Debemos obtener un código 200 si accedemos a la 
    # página de registro
    def test_register_redirect(self):
        tester = app.test_client(self)
        response = tester.get('/solved_problems', content_type="html/text")
        self.assertEqual(response.status_code, 200)

    # Prueba de muestra de los problemas resueltos por
    # un estudiante existente en la aplicación
    def test_solved_problems_by_existent_student(self):
        tester = app.test_client(self)
        student = "elalumno"
        response = tester.get('/solved_problems/'+student,
        content_type="html/text")
        self.assertIn('autor', response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 200)
    
    # Prueba de muestra de los problemas resueltos por
    # un estudiante inexistente en la aplicación
    def test_solved_problems_by_nonexistent_student(self):
        tester = app.test_client(self)
        student = "alumnoFalso"
        response = tester.get('/solved_problems/'+student,
        content_type="html/text")
        self.assertIn('', response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 200)

    # Prueba de muestra de los problemas resueltos por
    # un estudiante existente mediante un filtro de búsqueda
    # asociado un título de alguna relación
    def test_solved_problems_by_student_and_filter(self):
        tester = app.test_client(self)
        student = 'elalumno'
        filtrator = 'Reducción'
        response = tester.get('/solved_problems/'+student+
        '/'+filtrator,
        content_type="html/text")
        self.assertIn('Reduc', response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 200)

    # Prueba de muestra de los problemas resueltos por
    # un estudiante inexistente mediante un filtro de búsqueda
    # asociado un título de alguna relación
    def test_solved_problems_by_nonexistent_student_and_filter(self):
        tester = app.test_client(self)
        student = 'alumnoFalso'
        filtrator = 'Reducción'
        response = tester.get('/solved_problems/'+student+
        '/'+filtrator,
        content_type="html/text")
        self.assertIn('null', response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 200)

    # Prueba de muestra de los problemas resueltos por
    # un estudiante mediante un filtro de búsqueda no
    # asociado un título de alguna relación
    def test_solved_problems_by_student_and_filter_not_coinciding(self):
        tester = app.test_client(self)
        student = 'elalumno'
        filtrator = 'Jamones al vapor'
        response = tester.get('/solved_problems/'+student+
        '/'+filtrator,
        content_type="html/text")
        self.assertIn('[]', response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 200)

    # Prueba de muestra de las respuestas dadas por
    # un estudiante existente mediante su nombre de usuario
    # y el identificador de la relación
    def test_answers_by_student_and_relation_id(self):
        tester = app.test_client(self)
        student = 'elalumno'
        relation_id = '5ebecca15a540b78fd7274d9'
        response = tester.get('/solved_problems/responses/'+
        student+'/'+relation_id, content_type="html/text")
        self.assertIn('elalumno', response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 200)

    # Prueba de muestra de las respuestas dadas por
    # un estudiante existente mediante su nombre de usuario
    # y un identificador de relación inexistente
    def test_answers_by_student_and_invalid_relation_id(self):
        tester = app.test_client(self)
        student = 'elalumno'
        relation_id = 'jabugo fan numero uno'
        response = tester.get('/solved_problems/responses/'+
        student+'/'+relation_id, content_type="html/text")
        self.assertIn('null', response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 200)

    # Prueba de muestra de las respuestas dadas por
    # un estudiante existente mediante su nombre de usuario
    # y un identificador de relación inexistente
    def test_answers_by_student_and_invalid_relation_id(self):
        tester = app.test_client(self)
        student = 'elalumno'
        relation_id = 'jabugo fan numero uno'
        response = tester.get('/solved_problems/responses/'+
        student+'/'+relation_id, content_type="html/text")
        self.assertIn('null', response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 200)

    # Prueba de muestra de las respuestas dadas por
    # los estudiantes, dada un identificador de una
    # relación existente
    def test_answers_by_relation_id(self):
        tester = app.test_client(self)
        relation_id = '5ebecca15a540b78fd7274d9'
        response = tester.get('/solved_problems/id/'+
        relation_id, content_type="html/text")
        self.assertNotEqual('null', response.data.decode('utf-8'))
        self.assertNotEqual('[]', response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 200)

    # Prueba de muestra de las respuestas dadas por
    # los estudiantes, dada un identificador de una
    # relación inexistente
    def test_answers_by_nonexistent_relation_id(self):
        tester = app.test_client(self)
        relation_id = 'prueba'
        response = tester.get('/solved_problems/id/'+
        relation_id, content_type="html/text")
        self.assertEqual('null', response.data.decode('utf-8'))
        self.assertEqual(response.status_code, 200)


if __name__ == "__main__":
    unittest.main()
