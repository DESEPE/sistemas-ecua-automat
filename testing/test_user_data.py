import bson
import json
import os
import unittest

from flask import Flask
from pymongo import MongoClient

# Creado por Daniel Cruzado Poveda

# Obviamente necesitamos la aplicación para probarla
from main import app

class FlaskTest(unittest.TestCase):

    # Comprobamos si la conexión se hace correctamente
    def test_index(self):
        tester = app.test_client(self)
        response = tester.get('/', content_type="html/text")
        self.assertEqual(response.status_code, 200)

    # Debemos obtener un código 200 si accedemos a la 
    # página de perfil
    def test_profile_redirect(self):
        tester = app.test_client(self)
        response = tester.get('/profile', content_type="html/text")
        self.assertEqual(response.status_code, 200)

    # Prueba de búsqueda de un usuario mediante su 
    # nombre de usuario. Se introduce por consola el usuario
    def test_username_search(self):
        client = app.test_client(self)
        username = input('Introducir nombre de usuario de la aplicación \n')
        response = client.get('/user/username/'+username, 
        content_type='application/json')
        if username != "" or username != None:
            self.assertEqual(response.status_code, 200)
        else:
            self.assertEqual(response.status_code, 404)

    # Prueba de búsqueda de un usuario mediante 
    # datos vacíos. Debe devolver una error 404
    def test_empty_username_search(self):
        client = app.test_client(self)
        response = client.get('/user/username/', content_type='application/json')
        self.assertEqual(response.status_code, 404)

    # Prueba de búsqueda de un usuario mediante su 
    # email.
    def test_email_search(self):
        client = app.test_client(self)
        username = input('Introducir email de usuario de la aplicación \n')
        response = client.get('/user/email/'+username, 
        content_type='application/json')
        if username != "" or username != None:
            self.assertEqual(response.status_code, 200)
        else:
            self.assertEqual(response.status_code, 404)

    # Prueba de búsqueda de un usuario mediante 
    # datos vacíos. Debe devolver una error 404
    def test_empty_email_search(self):
        client = app.test_client(self)
        response = client.get('/user/email/', content_type='application/json')
        self.assertEqual(response.status_code, 404)

    # Prueba de búsqueda de un usuario mediante su 
    # email. El email se introduce vía consola
    def test_check_user_role(self):
        client = app.test_client(self)
        username = input('Introducir nombre de usuario de la aplicación \n')
        response = client.get('/user/role/'+username, 
        content_type='application/json')
        if username != "" or username != None:
            self.assertEqual(response.status_code, 200)
            response_data = response.data.decode('utf-8')
            if response_data == '"alumno"':
                self.assertIn('alumno', response_data)
            elif response_data == '"profesor"':
                self.assertIn('profesor', response_data)
        else:
            self.assertEqual(response.status_code, 404)

    Prueba la búsqueda de perfil de un usuario 
    registrado en la aplicación. El usuario es un alumno
    def test_profile_existing_student(self):
        client = app.test_client(self)
        username = 'elalumno'
        response = client.get('/profile/'+username,
        content_type='aplication/json')
        self.assertEqual(response.status_code, 200)
        response_data = (response.data.decode('utf-8'))
        self.assertIn('nombre_usuario', response_data)
        self.assertIn('nombre_completo', response_data)
        self.assertIn('email', response_data)
        # Esperamos que dentro esté el rol de alumno
        self.assertIn('"rol":"alumno"', response_data)
    
    # Prueba la búsqueda de perfil de un usuario 
    # registrado en la aplicación. El usuario es un profesor
    def test_profile_existing_professor(self):
        client = app.test_client(self)
        username = 'elprofesor'
        response = client.get('/profile/'+username,
        content_type='aplication/json')
        self.assertEqual(response.status_code, 200)
        response_data = (response.data.decode('utf-8'))
        self.assertIn('nombre_usuario', response_data)
        self.assertIn('nombre_completo', response_data)
        self.assertIn('email', response_data)
        # Esperamos que dentro esté el rol de alumno
        self.assertIn('"rol":"profesor"', response_data)

    # Prueba la búsqueda de perfil de un usuario
    # inexistente en la aplicación. Debe devolver
    # un error 404
    def test_profile_inexistent_user(self):
        client = app.test_client(self)
        username = 'perro'
        response = client.get('/profile/'+username,
        content_type='aplication/json')
        self.assertEqual(response.status_code, 404)

    # Prueba la búsqueda de perfil de un usuario
    # sin introducir usuario alguno. Debe devolver
    # un error 404
    def test_profile_empty_user(self):
        client = app.test_client(self)
        username = ''
        response = client.get('/profile/'+username,
        content_type='aplication/json')
        self.assertEqual(response.status_code, 404)

    Prueba de actualización de un alumno registrado
    en la aplicación.
    def test_profile_update_existing_student(self):
        client = app.test_client(self)
        data_sent = {
            'antiguo_usuario': 'antonio',
            'usuario': 'antonio',
            'contraseña': 'contraseñaAntonio',
            'email': 'antoniomail@mail.com',
            'rol': 'alumno'
        }
        response = client.put('/profile/antonio',
        data=json.dumps(data_sent), 
        content_type='application/json')
        self.assertEqual(response.status_code, 200)
    
    # Prueba de actualización de un profesor registrado
    # en la aplicación.
    def test_profile_update_existing_professor(self):
        client = app.test_client(self)
        data_sent = {
            'antiguo_usuario': 'profesor',
            'usuario': 'elprofesor',
            'contraseña': 'contraseñaprofe',
            'email': 'profemail@mail.com',
            'rol': 'profesor'
        }
        # Si no existe el usuario lo inserta, por lo que
        # es indicar un usuario que existe ya si lo que
        # queremos es actualizar
        response = client.put('/profile/profesor',
        data=json.dumps(data_sent), 
        content_type='application/json')
        self.assertEqual(response.status_code, 200)

    # Prueba de actualización de un usuario inexistente
    def test_profile_update_inexistent_user(self):
        client = app.test_client(self)
        data_sent = {
        }
        response = client.put("/profile/''",
        data=json.dumps(data_sent), 
        content_type='application/json')
        self.assertEqual(response.status_code, 400)


if __name__ == "__main__":
    unittest.main()
